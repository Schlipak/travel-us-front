import { spring } from 'react-router-transition';

const bounce = val => spring(val, {
  stiffness: 200,
  damping: 30,
});

export const slideLeftTransition = {
  atEnter: {
    opacity: 0,
    offset: 2,
  },
  atLeave: {
    opacity: bounce(0),
    offset: bounce(-2),
  },
  atActive: {
    opacity: bounce(1),
    offset: bounce(0),
  },
};

export const mapStyles = styles => ({
  opacity: styles.opacity,
  transform: `translateX(${styles.offset}%)`,
});

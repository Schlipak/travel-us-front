import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Titled } from 'react-titled';

import {
  Form, Input, Button, Checkbox, Tooltip,
} from 'antd';

import { withRouter } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { signUpUser } from '../redux/actions';

import Container from '../components/Container';
import FeatherIcon from '../components/FeatherIcon';

const HorizontalWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: ${props => props.justify || 'center'};
`;

const StyledForm = styled(Form)`
  width: calc(100% - 2em);
  max-width: 600px;

  .login-form-button {
    width: 100%;
  }

  .ant-form-item {
    .ant-form-item-label {
      > label > span {
        display: inline-flex;
        flex-direction: row;
        justify-content: flex-end;
        align-items: center;
      }
    }

    .ant-form-item-control {
      text-align: start;

      button {
        width: 100%;
      }
    }
  }
`;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

class SignUp extends Component {
  static propTypes = {
    form: PropTypes.objectOf(PropTypes.any).isRequired,

    currentUser: PropTypes.objectOf(PropTypes.any),
    loginCurrentUser: PropTypes.func.isRequired,

    history: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  static defaultProps = {
    currentUser: { loading: false },
  };

  constructor(props) {
    super();

    this.state = { confirmDirty: false };

    const { currentUser, history, location } = props;
    if (!currentUser.loading && currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  componentDidUpdate() {
    const { currentUser, history, location } = this.props;

    if (!currentUser.loading && currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  handleSubmit = (e) => {
    const { form, signUpUser: signUpUserAction } = this.props;

    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        const { username, password } = values;

        signUpUserAction(username, password);
      }
    });
  };

  handleConfirmBlur = (e) => {
    const { confirmDirty } = this.state;
    const { value } = e.target;

    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;

    if (value && value !== form.getFieldValue('password')) {
      callback('The passwords do not match');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    const { confirmDirty } = this.state;

    if (value && confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { form, currentUser } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Titled title={title => `${title} | Sign Up`}>
        <h2>Sign up</h2>
        <HorizontalWrapper>
          <StyledForm onSubmit={this.handleSubmit} className="login-form">
            <Form.Item
              {...formItemLayout}
              label={(
                <span>
                  Username&nbsp;
                  <Tooltip title="What do you want others to call you?">
                    <Container>
                      <FeatherIcon name="help-circle" color="rgba(0,0,0,.25)" />
                    </Container>
                  </Tooltip>
                </span>
              )}
            >
              {getFieldDecorator('username', {
                rules: [
                  { required: true, message: 'Please input your username!', whitespace: true },
                ],
              })(
                <Input
                  size="large"
                  prefix={<FeatherIcon name="at-sign" color="rgba(0,0,0,.25)" />}
                />,
              )}
            </Form.Item>
            <Form.Item {...formItemLayout} label="Password">
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                  {
                    validator: this.validateToNextPassword,
                  },
                ],
              })(<Input type="password" size="large" />)}
            </Form.Item>
            <Form.Item {...formItemLayout} label="Confirm Password">
              {getFieldDecorator('confirm', {
                rules: [
                  {
                    required: true,
                    message: 'Please confirm your password!',
                  },
                  {
                    validator: this.compareToFirstPassword,
                  },
                ],
              })(<Input type="password" onBlur={this.handleConfirmBlur} size="large" />)}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              {getFieldDecorator('agreement', {
                valuePropName: 'checked',
              })(
                <Checkbox>
                  I have read the&nbsp;
                  <a href="#placeholder">agreement</a>
                </Checkbox>,
              )}
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
              <Button
                type="primary"
                htmlType="submit"
                size="large"
                loading={currentUser.loading}
                disabled={currentUser.loading}
              >
                Register
              </Button>
            </Form.Item>
          </StyledForm>
        </HorizontalWrapper>
      </Titled>
    );
  }
}

const WrappedSignUp = Form.create()(SignUp);

const mapStateToProps = state => ({
  currentUser: state.currentUser,
});

const mapDispatchToProps = dispatch => bindActionCreators({ signUpUser }, dispatch);

const ConnectedWrappedSignUp = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(WrappedSignUp));

export default {
  path: '/sign_up',
  name: 'Sign up',
  exact: true,
  component: ConnectedWrappedSignUp,
};

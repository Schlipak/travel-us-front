import React from 'react';
import { Titled } from 'react-titled';

const NotFound = () => (
  <Titled title={title => `${title} | 404`}>
    <div>404 Not Found</div>
  </Titled>
);

export default {
  path: '*',
  name: 'Route not found',
  exact: false,
  component: NotFound,
};

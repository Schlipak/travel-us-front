import React from 'react';
import { Titled } from 'react-titled';

import { ReactComponent as Airport } from '../assets/images/airport.svg';

const Home = () => (
  <Titled title={title => title}>
    <Airport style={{ width: 'auto', height: 'auto', maxHeight: '75vmin' }} />
  </Titled>
);

export default {
  path: '/',
  name: 'Home',
  exact: true,
  component: Home,
};

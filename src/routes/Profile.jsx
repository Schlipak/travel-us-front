import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Titled } from 'react-titled';

import { Button } from 'antd';

import { withRouter } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { logoutCurrentUser } from '../redux/actions';

class Profile extends Component {
  static propTypes = {
    currentUser: PropTypes.objectOf(PropTypes.any),
    logoutCurrentUser: PropTypes.func.isRequired,

    history: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  static defaultProps = {
    currentUser: { loading: false },
  };

  constructor(props) {
    super();

    const { currentUser, history, location } = props;
    if (!currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  componentDidUpdate() {
    const { currentUser, history, location } = this.props;

    if (!currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  render() {
    const { currentUser, logoutCurrentUser: logoutCurrentUserAction } = this.props;

    return (
      <Titled title={title => `${title} | Login`}>
        {currentUser.user && <Button onClick={logoutCurrentUserAction}>Log out</Button>}
      </Titled>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.currentUser,
});

const mapDispatchToProps = dispatch => bindActionCreators({ logoutCurrentUser }, dispatch);

const ConnectedWrapperLogin = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(Profile));

export default {
  path: '/profile',
  name: 'Profile',
  exact: true,
  component: ConnectedWrapperLogin,
};

import Home from './Home';
import NotFound from './NotFound';
import Login from './Login';
import SignUp from './SignUp';
import Profile from './Profile';

const publicRoutes = [Home, Login, SignUp, Profile];
export default publicRoutes;

export const allRoutes = [...publicRoutes, NotFound];
export {
  Home, NotFound, Login, SignUp, Profile,
};

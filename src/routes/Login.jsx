import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Titled } from 'react-titled';

import {
  Form, Input, Button, Checkbox,
} from 'antd';

import { Link, withRouter } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { loginCurrentUser } from '../redux/actions';

import FeatherIcon from '../components/FeatherIcon';

const HorizontalWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: ${props => props.justify || 'center'};
`;

const StyledForm = styled(Form)`
  width: calc(100% - 2em);
  max-width: 600px;

  .login-form-button {
    width: 100%;
  }

  .ant-form-item {
    display: flex;
    flex-direction: column;
    align-items: stretch;

    .ant-form-item-label {
      text-align: start;
      line-height: 2;
    }
  }
`;

class Login extends Component {
  static propTypes = {
    form: PropTypes.objectOf(PropTypes.any).isRequired,

    currentUser: PropTypes.objectOf(PropTypes.any),
    loginCurrentUser: PropTypes.func.isRequired,

    history: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  static defaultProps = {
    currentUser: { loading: false },
  };

  constructor(props) {
    super();

    const { currentUser, history, location } = props;
    if (!currentUser.loading && currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  componentDidUpdate() {
    const { currentUser, history, location } = this.props;

    if (!currentUser.loading && currentUser.user && location.pathname !== '/') {
      history.push('/');
    }
  }

  handleSubmit = (e) => {
    const { form, loginCurrentUser: loginCurrentUserAction } = this.props;

    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        const { username, password } = values;

        loginCurrentUserAction(username, password);
      }
    });
  };

  render() {
    const { form, currentUser } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Titled title={title => `${title} | Login`}>
        <HorizontalWrapper>
          <StyledForm onSubmit={this.handleSubmit} className="login-form">
            <Form.Item label="Username">
              {getFieldDecorator('username', {
                rules: [{ required: true, message: 'Please input your username' }],
              })(
                <Input
                  prefix={<FeatherIcon name="user" color="rgba(0,0,0,.25)" />}
                  placeholder="Username"
                  size="large"
                />,
              )}
            </Form.Item>
            <Form.Item label="Password">
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your password' }],
              })(
                <Input
                  prefix={<FeatherIcon name="lock" color="rgba(0,0,0,.25)" />}
                  type="password"
                  placeholder="Password"
                  size="large"
                />,
              )}
            </Form.Item>
            <Form.Item>
              <HorizontalWrapper justify="space-between">
                {getFieldDecorator('remember', {
                  valuePropName: 'checked',
                  initialValue: true,
                })(<Checkbox>Remember me</Checkbox>)}
                <a className="login-form-forgot" href="#placeholder">
                  Forgot password
                </a>
              </HorizontalWrapper>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                size="large"
                loading={currentUser.loading}
                disabled={currentUser.loading}
              >
                Log in
              </Button>
              <HorizontalWrapper justify="flex-start">
                <span>Or&nbsp;</span>
                <Link to="/sign_up">Register now!</Link>
              </HorizontalWrapper>
            </Form.Item>
          </StyledForm>
        </HorizontalWrapper>
      </Titled>
    );
  }
}

const WrappedLogin = Form.create()(Login);

const mapStateToProps = state => ({
  currentUser: state.currentUser,
});

const mapDispatchToProps = dispatch => bindActionCreators({ loginCurrentUser }, dispatch);

const ConnectedWrappedLogin = connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(WrappedLogin));

export default {
  path: '/login',
  name: 'Log in',
  exact: true,
  component: ConnectedWrappedLogin,
};

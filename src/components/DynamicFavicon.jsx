import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class DynamicFavicon extends Component {
  static propTypes = {
    defaultIcon: PropTypes.string.isRequired,
    activeIcon: PropTypes.string.isRequired,
    count: PropTypes.number,
  };

  static defaultProps = {
    count: 0,
  };

  constructor(props) {
    super(props);

    const { defaultIcon } = this.props;

    this.state = {
      currentFavicon: defaultIcon,
    };
  }

  componentDidUpdate(prevProps) {
    const { count, defaultIcon, activeIcon } = this.props;

    if (prevProps.count === 0 && count > 0) {
      this.setCurrentFavicon(activeIcon);
    } else if (prevProps.count !== 0 && count === 0) {
      this.setCurrentFavicon(defaultIcon);
    }
  }

  setCurrentFavicon(uri) {
    this.setState({ currentFavicon: uri });
  }

  render() {
    const { currentFavicon } = this.state;

    return (
      <Helmet>
        <link rel="shortcut icon" href={currentFavicon} />
      </Helmet>
    );
  }
}

const mapStateToProps = state => ({
  count: state.counter.count,
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DynamicFavicon);

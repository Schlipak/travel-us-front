import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { List, Avatar } from 'antd';
import Moment from 'react-moment';

// import { bindActionCreators } from 'redux';
// import { connect } from 'react-redux';

import FeatherIcon from './FeatherIcon';

const MessageCard = styled(List.Item)`
  position: relative;
  flex-direction: column !important;
  align-items: flex-start !important;
  padding-left: 16px !important;
  padding-right: 16px !important;

  > .ant-list-item-meta {
    align-items: center !important;
  }

  > .ant-list-item-content {
    flex-direction: column !important;
    padding: 8px 0 8px 48px !important;

    > p {
      margin: 3px 0 !important;
      word-break: break-all;
    }
  }
`;

const IconText = ({ name, text }) => (
  <span>
    <FeatherIcon name={name} />
    <span style={{ marginLeft: 8 }}>{text}</span>
  </span>
);

IconText.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

const ChatConversationMessage = ({ message }) => (
  <MessageCard
    key={message.id}
    actions={[
      <IconText name="star" text="156" />,
      <IconText name="thumbs-up" text="156" />,
      <IconText name="message-circle" text="2" />,
    ]}
  >
    <List.Item.Meta
      avatar={<Avatar src={`https://picsum.photos/200/200/?random&v=${message.userId}`} />}
      title={(
        <span>
          User:
          {message.userId}
        </span>
      )}
      description={
        <Moment date={message.timestamp} interval={10000} fromNow fromNowDuring={60000} />
      }
    />
    {message.content.map((msg, i) => (
      /* eslint-disable-next-line react/no-array-index-key */
      <p key={i}>{msg}</p>
    ))}
  </MessageCard>
);

ChatConversationMessage.propTypes = {
  message: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default ChatConversationMessage;

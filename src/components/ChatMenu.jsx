import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, Menu, Drawer } from 'antd';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { NavLink } from 'react-router-dom';

import { fetchRooms, fetchConversation } from '../redux/actions';

const StyledMenu = styled(Menu)`
  min-height: 100%;

  @media only screen and (max-width: 900px) {
    display: none;
  }
`;

const DrawerContainer = styled.div`
  display: none;
  position: relative;

  @media only screen and (max-width: 900px) {
    display: unset;
  }

  > button {
    pointer-events: all;
  }
`;

const StyledDrawer = styled(Drawer)`
  display: none;

  @media only screen and (max-width: 900px) {
    display: unset;
  }
`;

class ChatMenu extends Component {
  static propTypes = {
    rooms: PropTypes.objectOf(PropTypes.any),
    fetchRooms: PropTypes.func.isRequired,
    fetchConversation: PropTypes.func.isRequired,
  };

  static defaultProps = {
    rooms: { loading: false, elements: [] },
  };

  constructor() {
    super();

    this.state = { visible: false };
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  closeAndFetch(id) {
    const { fetchConversation: fetchConversationAction } = this.props;

    fetchConversationAction(id);
    setTimeout(this.onClose, 500);
  }

  render() {
    const {
      rooms,
      fetchRooms: fetchRoomsAction,
      fetchConversation: fetchConversationAction,
    } = this.props;

    const { visible } = this.state;

    return (
      <>
        <StyledMenu mode="inline" defaultOpenKeys={['rooms']}>
          <Menu.Item>
            <Button onClick={fetchRoomsAction}>
              {rooms.loading ? 'Fetching rooms...' : 'Fetch rooms'}
            </Button>
          </Menu.Item>
          <Menu.SubMenu key="rooms" title="Conversation rooms">
            {/* TODO: Sort by last activity timestamp */}
            {rooms.elements.sort((a, b) => a.name > b.name).map(room => (
              <Menu.Item key={room.id}>
                <NavLink to={`/room/${room.id}`} onClick={() => fetchConversationAction(room.id)}>
                  {room.name}
                </NavLink>
              </Menu.Item>
            ))}
          </Menu.SubMenu>
        </StyledMenu>
        <DrawerContainer>
          <StyledDrawer
            title="Chat menu"
            placement="left"
            onClose={this.onClose}
            visible={visible}
            style={{ padding: 0 }}
            closable
          >
            <Menu mode="inline" defaultOpenKeys={['rooms']}>
              <Menu.SubMenu key="rooms" title="Conversation rooms">
                {rooms.elements.sort((a, b) => a.name > b.name).map(room => (
                  <Menu.Item key={room.id}>
                    <NavLink to={`/room/${room.id}`} onClick={() => this.closeAndFetch(room.id)}>
                      {room.name}
                    </NavLink>
                  </Menu.Item>
                ))}
              </Menu.SubMenu>
            </Menu>
          </StyledDrawer>
        </DrawerContainer>
      </>
    );
  }
}

const mapStateToProps = state => ({
  rooms: state.rooms,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchRooms,
    fetchConversation,
  },
  dispatch,
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true },
)(ChatMenu);

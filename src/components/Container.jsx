import styled, { css } from 'styled-components';

export default styled.div`
  display: flex;
  position: ${props => props.position || 'relative'};
  flex-direction: ${props => props.direction || 'row'};
  justify-content: ${props => props.justifyContent || 'center'};
  align-items: ${props => props.alignItems || 'center'};

  top: ${props => props.top || 'unset'};
  bottom: ${props => props.bottom || 'unset'};
  left: ${props => props.left || 'unset'};
  right: ${props => props.right || 'unset'};

  ${props => props.expand
    && css`
      width: 100%;
    `};

  ${props => props.spacing
    && css`
      > *:not(:first-child) {
        margin-left: ${props.spacing};
      }

      > *:not(:last-child) {
        margin-right: ${props.spacing};
      }
    `};
`;

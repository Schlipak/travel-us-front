import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FeatherIcon from './FeatherIcon';

import { sendMessage, socketSend } from '../redux/actions';

import Audio from './Audio';
import SentSound from '../assets/sounds/pop_sent.mp3';

const InputContainer = styled.form`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  flex-shrink: 0;
  width: 100%;
`;

const StyledInput = styled.input`
  width: 100%;
  border: none;
  border-top: 1px solid #e8e8e8;
  padding: 0.75em 1.25em;

  outline: 0 solid transparent;
  transition: border-color 0.25s, outline 0.35s;
  transition-timing-function: ease-in-out;

  &:hover,
  &:focus {
    border-top-color: #20bf6b;
  }

  &:focus {
    outline: 6px solid rgba(32, 191, 107, 0.1);
  }
`;

const StyledButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 1em;

  font-size: 1.25em;
  background-color: #20bf6b;
  color: white;
  border: none !important;
  outline: 7px solid transparent;
  opacity: 1;

  cursor: pointer;
  transition: background-color 0.25s, outline 0.5s, opacity 0.25s;
  transition-timing-function: ease-in-out;

  &:hover,
  &:focus {
    background-color: #39ca82;
  }

  &:active {
    transition: background-color 0.1s;
    background-color: #00975a;
    outline: 0 solid rgba(32, 191, 107, 0.5);
  }

  &:disabled {
    pointer-events: none;
    opacity: 0.5;
  }
`;

class ChatConversationInput extends Component {
  static propTypes = {
    room: PropTypes.objectOf(PropTypes.any).isRequired,

    sendMessage: PropTypes.func.isRequired,
    socketSend: PropTypes.func.isRequired,

    onSend: PropTypes.func,
  };

  static defaultProps = {
    onSend: null,
  };

  constructor() {
    super();
    this.state = { message: '' };
  }

  handleSubmit = (event) => {
    const { room, sendMessage: sendMessageAction, socketSend: socketSendAction } = this.props;
    const { message } = this.state;

    sendMessageAction(message, room.id, this.notifySent);
    socketSendAction({ type: 'CHAT_NEW_MESSAGE', body: message });

    this.setState({ message: '' });

    event.preventDefault();
    return false;
  };

  updateMessage = (event) => {
    this.setState({ message: event.target.value });
  };

  notifySent = () => {
    const { onSend } = this.props;

    if (this.audio) this.audio.play();
    if (onSend) onSend();
  };

  render() {
    const { room } = this.props;
    const { message } = this.state;

    return (
      <InputContainer onSubmit={this.handleSubmit}>
        <StyledInput
          type="text"
          placeholder={`Message #${room.name}`}
          value={message}
          onChange={event => this.updateMessage(event)}
        />
        <StyledButton type="submit" disabled={!message || message.length === 0}>
          <FeatherIcon name="send" />
        </StyledButton>
        <Audio
          sources={[{ src: SentSound }]}
          ref={(audio) => {
            this.audio = audio;
          }}
        />
      </InputContainer>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => bindActionCreators({ sendMessage, socketSend }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChatConversationInput);

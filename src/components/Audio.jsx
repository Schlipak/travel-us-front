import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Audio extends Component {
  static propTypes = {
    sources: PropTypes.arrayOf(PropTypes.object).isRequired,
    autoplay: PropTypes.bool,
    controls: PropTypes.bool,
    loop: PropTypes.bool,
  };

  static defaultProps = {
    autoplay: false,
    controls: false,
    loop: false,
  };

  play() {
    this.audio.play();
  }

  render() {
    const {
      sources, autoplay, controls, loop,
    } = this.props;

    return (
      /* eslint-disable jsx-a11y/media-has-caption */
      <audio
        ref={(audio) => {
          this.audio = audio;
        }}
        autoPlay={autoplay}
        controls={controls}
        loop={loop}
        preload="auto"
      >
        {sources.map(source => (
          <source key={source.src} src={source.src} type={source.type || 'audio/mp3'} />
        ))}
      </audio>
      /* eslint-enable jsx-a11y/media-has-caption */
    );
  }
}

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { ReactComponent as Logo } from '../assets/images/logo.svg';
import Container from './Container';

const StyledHeader = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  position: relative;
  top: 0;
  left: 0;
  width: 100%;
  padding: 2em;

  color: #2f3542;
  background-color: #ffffff;
  box-shadow: 0 0.5em 0.5em -0.25em rgba(0, 0, 0, 0.1);

  z-index: 900;

  @media only screen and (max-width: 900px) {
    padding: 1em;
  }

  > .header-content-wrapper {
    max-width: 1200px;

    @media only screen and (max-width: 900px) {
      max-width: unset;
    }

    svg.header-logo {
      height: 3em;
      width: auto;
      color: #26de81;
    }

    h1.header-title {
      position: relative;
      margin: 0;
      font-weight: 500;
      line-height: 1em;
    }
  }
`;

const HeaderHomeLink = styled(Link)`
  text-decoration: none !important;
  color: currentColor;
`;

const Header = ({ children }) => (
  <StyledHeader>
    <Container
      className="header-content-wrapper"
      justifyContent="space-between"
      alignItems="center"
      expand
    >
      <HeaderHomeLink to="/">
        <Container alignItems="baseline" spacing="0.25em">
          <Logo className="header-logo" />
          <h1 className="header-title">TravelUs</h1>
        </Container>
      </HeaderHomeLink>
      {children}
    </Container>
  </StyledHeader>
);

Header.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Header;

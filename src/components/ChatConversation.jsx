import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Spin, List } from 'antd';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { withRouter } from 'react-router-dom';

import ChatConversationMessage from './ChatConversationMessage';
import ChatConversationInput from './ChatConversationInput';

const ChatConversationContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: space-between;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

const ChatConversationMessagesContainer = styled(List)`
  position: relative;
  width: 100%;
  flex-grow: 1;
  overflow-y: auto;
`;

const SpinContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;

// TODO: FIXME:
// Write a Saga to load only the messages of a given room
class ChatConversation extends Component {
  static propTypes = {
    room: PropTypes.objectOf(PropTypes.any).isRequired,
    conversation: PropTypes.objectOf(PropTypes.any),
    history: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any).isRequired,
  };

  static defaultProps = {
    conversation: { id: 0, messages: [] },
  };

  constructor() {
    super();

    this.node = null;
    this.previousBounds = { top: 0, height: 0 };
  }

  componentDidMount() {
    this.scrollToBottom(true);
  }

  componentDidUpdate() {
    const {
      room, conversation, history, location,
    } = this.props;

    if (conversation && !conversation.loading && location.pathname !== `/room/${room.id}/loaded`) {
      history.push(`/room/${room.id}/loaded`);
    }

    this.scrollToBottom();
  }

  getSnapshotBeforeUpdate() {
    if (this.node) {
      this.previousBounds = { top: this.node.scrollTop, height: this.node.scrollHeight };
    }

    return null;
  }

  scrollToBottom = (force = false) => {
    if (this.messagesContainer && !this.node) {
      /* eslint-disable-next-line react/no-find-dom-node */
      this.node = ReactDOM.findDOMNode(this.messagesContainer);
    }

    if (this.node) {
      if (
        force
        || this.previousBounds.top + this.node.offsetHeight >= this.previousBounds.height - 70
      ) {
        this.node.scrollTop = this.node.scrollHeight;
      }
    }
  };

  renderConversation = () => {
    const { room, conversation } = this.props;

    /* eslint-disable no-param-reassign */
    const messages = conversation.messages.reduce((acc, msg) => {
      const clone = { ...msg };

      clone.content = [clone.content];

      if (acc.length && acc[acc.length - 1].userId === clone.userId) {
        const last = acc[acc.length - 1];

        acc[acc.length - 1].content = [...last.content, ...clone.content];
        acc[acc.length - 1].timestamp = clone.timestamp;
        return acc;
      }

      return [...acc, clone];
    }, []);
    /* eslint-enable no-param-reassign */

    return (
      <ChatConversationContainer>
        <ChatConversationMessagesContainer
          ref={(messagesContainer) => {
            this.messagesContainer = messagesContainer;
          }}
          itemLayout="horizontal"
          dataSource={messages}
          renderItem={item => <ChatConversationMessage message={item} />}
        />
        <ChatConversationInput
          room={room}
          onSend={() => {
            this.scrollToBottom(true);
          }}
        />
      </ChatConversationContainer>
    );
  };

  render() {
    const { room, location } = this.props;

    if (location.pathname === `/room/${room.id}/loaded`) {
      return this.renderConversation();
    }

    return (
      <SpinContainer>
        <Spin />
      </SpinContainer>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  conversation: state.conversations.find(conv => conv.roomId === ownProps.room.id),
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withRouter(ChatConversation));

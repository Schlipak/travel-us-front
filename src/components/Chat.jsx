import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import {
  Row, Col, Collapse, Badge, Button,
} from 'antd';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { MemoryRouter as Router, Route } from 'react-router-dom';
import { AnimatedSwitch } from 'react-router-transition';
import { slideLeftTransition, mapStyles } from '../transitions/slideLeftTransition';

import { fetchConversation, createSocket, closeSocket } from '../redux/actions';

import Container from './Container';
import FeatherIcon from './FeatherIcon';

import Audio from './Audio';
import NotificationSound from '../assets/sounds/kalimba_klick.mp3';
import ChatMenu from './ChatMenu';
import ChatConversation from './ChatConversation';

const ChatContainer = styled(Collapse)`
  position: fixed;
  bottom: 0;
  right: 0;
  background-color: #fff;
  min-width: 900px;
  max-width: 60vw;
  width: 100vw;

  border-radius: 0em;
  border-top-left-radius: 4px;
  box-shadow: 0 0 0.5em -0.25em rgba(0, 0, 0, 0.1);

  transition: min-width 0.2s, max-width 0.2s;
  will-change: min-width, max-width;
  z-index: 950;

  ${props => props.collapsed
    && css`
      min-width: 300px;
      max-width: 300px;
    `} @media only screen and (max-width: 900px) {
    min-width: 100vw;
    max-width: unset;
    border-top-left-radius: 0;
  }

  > .ant-collapse-item {
    border-bottom: none !important;
  }

  .chat-content-container {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    align-items: stretch;
    height: 75vh;

    @media only screen and (max-width: 900px) {
      /* Viewport height - chat header contents - chat header padding */
      height: calc(100vh - 2.3em - 24px);

      @media only screen and (-webkit-min-device-pixel-ratio: 0) and (min-resolution: 0.001dpcm) {
        @supports (-webkit-appearance: none) {
          height: calc(100vh - 2.3em - 24px - 56px);
        }
      }
    }

    > .chat-room-container {
      height: 100%;
      overflow-y: auto;

      @media only screen and (max-width: 900px) {
        position: absolute;
        overflow: visible;
        z-index: 100;
        pointer-events: none;
      }
    }

    > .chat-conversation-container {
      height: 100%;
      max-height: 100%;
      overflow: hidden;

      @media only screen and (max-width: 900px) {
        width: 100%;
      }

      > .chat-conversation-switch {
        width: 100%;
        height: 100%;
        max-width: 100%;
        max-height: 100%;
        overflow: hidden;

        > div {
          position: absolute;
          width: 100%;
          height: 100%;
          max-width: 100%;
          max-height: 100%;
          overflow: hidden;
        }
      }
    }
  }
`;

const ChatPanel = styled(Collapse.Panel)`
  padding: 0;

  > .ant-collapse-header {
    padding-right: 16px !important;

    color: #fff !important;
    background-color: #2f3542;
    border-top-left-radius: 4px !important;

    @media only screen and (max-width: 900px) {
      border-top-left-radius: 0 !important;

      ${props => !props.collapsed
        && css`
          padding-left: 0 !important;
        `};
    }

    h3 {
      color: #fff !important;
    }
  }

  > .ant-collapse-content {
    > .ant-collapse-content-box {
      padding: 0 !important;
    }
  }
`;

const ChatTitleContainer = styled.h3`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin: 0;
  line-height: 1em;

  > button {
    display: none;
  }

  @media only screen and (max-width: 900px) {
    > button {
      display: unset;
    }
  }

  > svg {
    transition-property: color, fill, stroke;
    transition-duration: 0.2s;
  }

  > span:not(.ant-badge) {
    margin-left: 0.5em;
    line-height: 1em;
  }

  > span.ant-badge > sup {
    @media only screen and (max-width: 900px) {
      right: 25px !important;
      margin-top: 0px !important;
    }
  }
`;

const DefaultChatPage = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;

  opacity: 0.25;
  pointer-events: none;
  user-select: none;

  > svg {
    font-size: 3em;
  }

  > span {
    margin-top: 0.5em;
    font-size: 1.5em;
  }
`;

const MenuDrawerButton = styled(Button)`
  transition-property: max-width, margin, padding, opacity;
  transition-duration: 0.2s;
  will-change: max-width, margin, padding;

  color: white !important;
  background-color: transparent !important;
  border: none !important;

  ${props => props.collapsed
    && css`
      max-width: 0 !important;
      margin: 0 !important;
      padding: 0 !important;
      opacity: 0 !important;
      pointer-events: none;
    `};
`;

class Chat extends Component {
  static propTypes = {
    count: PropTypes.number,
    rooms: PropTypes.objectOf(PropTypes.any),

    createSocket: PropTypes.func.isRequired,
    closeSocket: PropTypes.func.isRequired,
  };

  static defaultProps = {
    count: 0,
    rooms: { loading: false, elements: [] },
  };

  constructor() {
    super();
    this.state = {
      collapsed: true,
    };
  }

  componentDidMount() {
    const { createSocket: createSocketAction } = this.props;

    createSocketAction();
  }

  componentDidUpdate(prevProps) {
    const { count } = this.props;

    if (prevProps.count < count) {
      this.audio.play();
    }
  }

  componentWillUnmount() {
    const { closeSocket: closeSocketAction } = this.props;

    closeSocketAction();
  }

  renderChatHeader = () => {
    const { count } = this.props;
    const { collapsed } = this.state;

    return (
      <Container justifyContent="space-between" expand>
        <ChatTitleContainer>
          <Badge count={count} offset={[-12, -18]}>
            <></>
          </Badge>
          <MenuDrawerButton
            collapsed={collapsed ? 1 : 0}
            onClick={(e) => {
              e.stopPropagation();
              if (this.menu) {
                const instance = this.menu.getWrappedInstance();

                if (instance) instance.showDrawer();
              }
            }}
          >
            <FeatherIcon name="menu" />
          </MenuDrawerButton>
          <FeatherIcon
            name="message-circle"
            // TODO: Set with unread count
            filled={count > 0}
            color={count > 0 ? '#26de81' : undefined}
          />
          <span>Chat</span>
        </ChatTitleContainer>
        <FeatherIcon name={collapsed ? 'maximize-2' : 'minimize-2'} size="1em" />
      </Container>
    );
  };

  render() {
    const { rooms } = this.props;
    const { collapsed } = this.state;

    return (
      <>
        <ChatContainer
          onChange={opened => this.setState({ collapsed: !opened })}
          bordered={false}
          collapsed={collapsed}
          accordion
        >
          <ChatPanel
            header={this.renderChatHeader()}
            showArrow={false}
            collapsed={collapsed ? 1 : 0}
            key="1"
          >
            <Router>
              <Row className="chat-content-container">
                <Col className="chat-room-container" span={6}>
                  <ChatMenu
                    ref={(menu) => {
                      this.menu = menu;
                    }}
                  />
                </Col>
                <Col className="chat-conversation-container" span={18}>
                  <AnimatedSwitch
                    atEnter={slideLeftTransition.atEnter}
                    atLeave={slideLeftTransition.atLeave}
                    atActive={slideLeftTransition.atActive}
                    mapStyles={mapStyles}
                    className="chat-conversation-switch"
                  >
                    {rooms.elements.map(room => (
                      <Route
                        key={`${room.id}.loading`}
                        path={`/room/${room.id}`}
                        render={() => <ChatConversation room={room} />}
                        exact
                      />
                    ))}
                    {rooms.elements.map(room => (
                      <Route
                        key={`${room.id}.loaded`}
                        path={`/room/${room.id}/loaded`}
                        render={() => <ChatConversation room={room} />}
                        exact
                      />
                    ))}
                    <Route
                      render={() => (
                        <DefaultChatPage>
                          <FeatherIcon name="message-circle" />
                          <span>Chat</span>
                        </DefaultChatPage>
                      )}
                    />
                  </AnimatedSwitch>
                </Col>
              </Row>
            </Router>
          </ChatPanel>
        </ChatContainer>
        <Audio
          sources={[{ src: NotificationSound }]}
          ref={(audio) => {
            this.audio = audio;
          }}
        />
      </>
    );
  }
}

const mapStateToProps = state => ({
  count: state.counter.count,
  rooms: state.rooms,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchConversation,
    createSocket,
    closeSocket,
  },
  dispatch,
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chat);

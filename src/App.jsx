import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, Avatar } from 'antd';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Titled } from 'react-titled';

import { Router, Route, Link } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import { AnimatedSwitch } from 'react-router-transition';

import {
  fetchCurrentUser,
  incrementCount,
  resetCount,
} from './redux/actions';
import { Status as CurrentUserStatus } from './redux/reducers/currentUser';

import { slideLeftTransition, mapStyles } from './transitions/slideLeftTransition';
import { allRoutes } from './routes';

import DynamicFavicon from './components/DynamicFavicon';
import Header from './components/Header';
import Chat from './components/Chat';

const history = createHistory();

const AppStage = styled.main`
  display: block;
  position: relative;
  overflow: hidden;
  height: 100%;

  > .scrollable-container {
    position: relative;
    width: 100vw;
    height: calc(100% - 7em);
    overflow-y: auto;

    @media only screen and (max-width: 900px) {
      height: calc(100% - 5em);
    }

    > .route-wrapper > div {
      position: absolute;
      width: 100%;
      padding: 3em 0;
      text-align: center;
    }
  }
`;

const ProfileLink = styled(Link)`
  > span {
    margin-right: 1em;
  }
`;

const DebugArea = styled.div`
  display: block;
  position: fixed;
  bottom: 0;
  left: 0;
  padding: 0.75em;

  font-family: 'Fira Code', 'Courier New', Courier, monospace;
  background-color: white;
  box-shadow: 0 0 1em rgba(0, 0, 0, 0.25);
  opacity: 1;
  transition: opacity 0.2s;

  z-index: 999;

  &:hover {
    opacity: 0.25;
  }
`;

class App extends Component {
  static propTypes = {
    currentUser: PropTypes.objectOf(PropTypes.any),
    count: PropTypes.number,

    chatSocket: PropTypes.objectOf(PropTypes.any),

    fetchCurrentUser: PropTypes.func.isRequired,
    incrementCount: PropTypes.func.isRequired,
    resetCount: PropTypes.func.isRequired,
  };

  static defaultProps = {
    currentUser: {},
    count: 0,

    chatSocket: {},
  };

  componentDidMount() {
    const { fetchCurrentUser: fetchCurrentUserAction } = this.props;

    fetchCurrentUserAction();
  }

  render() {
    const {
      currentUser,
      count,

      chatSocket,

      incrementCount: incrementCountAction,
      resetCount: resetCountAction,
    } = this.props;

    return (
      <>
        <DynamicFavicon defaultIcon="/favicon.png" activeIcon="/favicon_notify.png" />
        <Titled
          title={() => {
            if (count > 0) {
              if (count <= 99) return `(${count}) TravelUs`;
              return '(99+) TravelUs';
            }

            return 'TravelUs';
          }}
        >
          <Router history={history}>
            <AppStage className="app-stage">
              <Header>
                <Button.Group size="small">
                  <Button onClick={incrementCountAction} type="primary">
                    +
                  </Button>
                  <Button onClick={resetCountAction}>0</Button>
                </Button.Group>
                {currentUser.status === CurrentUserStatus.LOGGED_IN && currentUser.user ? (
                  <ProfileLink to="/profile">
                    <span>{currentUser.user.username}</span>
                    <Avatar src={currentUser.user.image} size="large">
                      {currentUser.user.username
                        .split(/\s/)
                        .map(w => w[0].toUpperCase())
                        .join('')}
                    </Avatar>
                  </ProfileLink>
                ) : (
                  <Link to="/login">Log in</Link>
                )}
              </Header>
              <div className="scrollable-container">
                <AnimatedSwitch
                  atEnter={slideLeftTransition.atEnter}
                  atLeave={slideLeftTransition.atLeave}
                  atActive={slideLeftTransition.atActive}
                  mapStyles={mapStyles}
                  className="route-wrapper"
                >
                  {allRoutes.map(route => (
                    <Route
                      key={route.name}
                      exact={route.exact || false}
                      path={route.path}
                      render={() => <route.component history={history} />}
                    />
                  ))}
                </AnimatedSwitch>
              </div>
            </AppStage>
          </Router>
          {currentUser.status === CurrentUserStatus.LOGGED_IN && currentUser.user && <Chat />}
        </Titled>
        <DebugArea>
          <div>
            User =&gt;&nbsp;
            {currentUser ? JSON.stringify(currentUser) : '(NONE)'}
          </div>
          <div>
            Sock =&gt;&nbsp;
            {chatSocket ? JSON.stringify(chatSocket) : '(NONE)'}
          </div>
        </DebugArea>
      </>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.currentUser,
  count: state.counter.count,

  chatSocket: state.chatSocket,
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    fetchCurrentUser,
    incrementCount,
    resetCount,
  },
  dispatch,
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

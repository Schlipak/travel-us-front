import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import store from './redux';

import 'antd/dist/antd.less';
import './styles/index.scss';

import App from './App';
import * as serviceWorker from './serviceWorker';

render(
  /* eslint-disable react/jsx-filename-extension */
  <Provider store={store}>
    <App />
  </Provider>,
  /* eslint-enable react/jsx-filename-extension */
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

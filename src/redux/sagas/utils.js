import { eventChannel } from 'redux-saga';

export function later(delay, callback) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (callback) {
        callback(resolve, reject);
      } else {
        resolve(delay);
      }
    }, delay);
  });
}

export async function after(promise, callback) {
  return callback(await promise);
}

export async function fetchEndpoint(endpoint, options = {}, defaultOptions = {}) {
  const { bodyType = 'json', defaultValue, ...fetchOptions } = options;

  let response;
  try {
    response = await fetch(endpoint, { ...defaultOptions, ...fetchOptions });

    if (response.ok) return response[bodyType]();
    throw new Error(`Failed to fetch endpoint: Request ended in ${response.status}`);
  } catch (error) {
    return typeof defaultValue === 'undefined' ? response[bodyType]() : defaultValue;
  }
}

export function httpGet(endpoint, options = {}) {
  return fetchEndpoint(endpoint, options, { method: 'GET' });
}

export function httpPost(endpoint, options = {}) {
  return fetchEndpoint(endpoint, options, { method: 'POST' });
}

export function httpPut(endpoint, options = {}) {
  return fetchEndpoint(endpoint, options, { method: 'PUT' });
}

export function httpDelete(endpoint, options = {}) {
  return fetchEndpoint(endpoint, options, { method: 'DELETE' });
}

export function httpPatch(endpoint, options = {}) {
  return fetchEndpoint(endpoint, options, { method: 'PATCH' });
}

const DEFAULT_WS_HANDLER = name => () => {
  /* eslint-disable-next-line no-console */
  console.log(`WebSocket callback ${name} not implemented`);
};

export function websocketInitChannel(
  endpoint,
  onopen = DEFAULT_WS_HANDLER('opoen'),
  onmessage = DEFAULT_WS_HANDLER('onmessage'),
  onclose = DEFAULT_WS_HANDLER('onclose'),
  onerror = DEFAULT_WS_HANDLER('onerror'),
) {
  return eventChannel((emitter) => {
    const ws = new WebSocket(endpoint);

    ws.onopen = event => onopen(emitter, ws, event);
    ws.onmessage = event => onmessage(emitter, ws, event);
    ws.onerror = event => onerror(emitter, ws, event);
    ws.onclose = event => onclose(emitter, ws, event);

    return () => {
      ws.close(0, 'EventChannel finished');
    };
  });
}

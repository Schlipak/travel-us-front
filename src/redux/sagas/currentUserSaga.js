import { put, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';

import {
  FETCH_CURRENT_USER_REQUESTED,
  FETCH_CURRENT_USER_SUCCESS,
  LOGOUT_CURRENT_USER_REQUESTED,
  LOGOUT_CURRENT_USER_SUCCESS,
  LOGIN_CURRENT_USER_REQUESTED,
  LOGIN_CURRENT_USER_SUCCESS,
  LOGIN_CURRENT_USER_ERROR,
  FETCH_CURRENT_USER_ERROR,
  LOGOUT_CURRENT_USER_ERROR,
  SIGN_UP_CURRENT_USER_REQUESTED,
  SIGN_UP_CURRENT_USER_SUCCESS,
  SIGN_UP_CURRENT_USER_ERROR,
} from '../actions/types';

import { httpGet, httpPost } from './utils';

export function* fetchCurrentUser() {
  const response = yield httpGet('/auth/status');

  if (response && response.code === 200) {
    yield put({
      type: FETCH_CURRENT_USER_SUCCESS,
      payload: { user: response.user },
    });
  } else {
    yield put({
      type: FETCH_CURRENT_USER_ERROR,
    });
  }
}

export function* loginCurrentUser(action) {
  const { username, password } = action.payload;
  const formData = `username=${username}&password=${password}`;

  const response = yield httpPost('/auth/login', {
    headers: {
      Accept: 'application/json, application/xml, text/plain, text/html, *.*',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formData,
  });

  if (response && response.code === 200) {
    yield put({
      type: LOGIN_CURRENT_USER_SUCCESS,
      payload: { user: response.user },
    });
  } else {
    message.error('Invalid username or password');
    yield put({
      type: LOGIN_CURRENT_USER_ERROR,
    });
  }
}

export function* logoutCurrentUser() {
  const response = yield httpPost('/auth/logout');

  if (response && response.code === 200) {
    yield put({
      type: LOGOUT_CURRENT_USER_SUCCESS,
    });
  } else {
    yield put({
      type: LOGOUT_CURRENT_USER_ERROR,
    });
  }
}

export function* signUpUser(action) {
  const { username, password } = action.payload;
  const formData = `username=${username}&password=${password}`;

  const response = yield httpPost('/auth/register', {
    headers: {
      Accept: 'application/json, application/xml, text/plain, text/html, *.*',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: formData,
  });

  if (response && response.code === 200) {
    yield put({
      type: SIGN_UP_CURRENT_USER_SUCCESS,
      payload: { user: response.user },
    });
  } else {
    message.error(`Error while creating the account: ${response.message}`);

    yield put({
      type: SIGN_UP_CURRENT_USER_ERROR,
    });
  }
}

export default function* roomsSaga() {
  yield takeLatest(FETCH_CURRENT_USER_REQUESTED, fetchCurrentUser);
  yield takeLatest(LOGIN_CURRENT_USER_REQUESTED, loginCurrentUser);
  yield takeLatest(LOGOUT_CURRENT_USER_REQUESTED, logoutCurrentUser);
  yield takeLatest(SIGN_UP_CURRENT_USER_REQUESTED, signUpUser);
}

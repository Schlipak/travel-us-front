import { fork, all } from 'redux-saga/effects';

import currentUserSaga from './currentUserSaga';
import chatSocketSaga from './chatSocketSaga';
import incrementSaga from './incrementSaga';
import roomsSaga from './roomsSaga';
import conversationSaga from './conversationSaga';
import outgoingMessageSaga from './outgoingMessageSaga';

export default function* rootSaga() {
  yield all([
    fork(currentUserSaga),
    fork(chatSocketSaga),
    fork(incrementSaga),
    fork(roomsSaga),
    fork(conversationSaga),
    fork(outgoingMessageSaga),
  ]);
}

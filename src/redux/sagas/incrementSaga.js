import { put, takeLatest } from 'redux-saga/effects';
import { later } from './utils';

import { INCREMENT_COUNT, INCREMENT_COUNT_DONE } from '../actions/types';

export function* incrementLater(/* action */) {
  yield later(1000);
  yield put({ type: INCREMENT_COUNT_DONE });
}

export default function* incrementSaga() {
  yield takeLatest(INCREMENT_COUNT, incrementLater);
}

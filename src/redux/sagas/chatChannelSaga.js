import {
  take, put, call, cancelled,
} from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';

import { websocketInitChannel } from './utils';

export function* chatChannelSaga() {
  const chan = yield call(websocketInitChannel);

  try {
    while (true) {
      const message = yield take(chan);
      /* eslint-disable-next-line no-console */
      console.log(message);
    }
  } finally {
    /* eslint-disable-next-line no-console */
    if (yield cancelled()) {
      chan.close();
      console.log('End');
    }
  }
}

import {
  call, put, take, takeLatest, takeEvery,
} from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';

import {
  CREATE_SOCKET_REQUESTED,
  CREATE_SOCKET_SUCCESS,
  CREATE_SOCKET_ERROR,
  CLOSE_SOCKET_REQUESTED,
  CLOSE_SOCKET_SUCCESS,
  SOCKET_MESSAGE_RECEIVED,
  SOCKET_SEND_REQUESTED,
  SOCKET_SEND_SUCCESS,
  SOCKET_SEND_ERROR,
} from '../actions/types';

let sock = null;

const initSockChannel = function initSockChannel() {
  return eventChannel((emitter) => {
    // eslint-disable-next-line no-console
    console.log('Creating Socket');

    const host = window.location.origin.replace(/^http/, 'ws');
    sock = new WebSocket(`${host}/ping`);

    sock.addEventListener('open', () => {
      // eslint-disable-next-line no-console
      console.log('Socket UP');

      sock.addEventListener('message', (event) => {
        // eslint-disable-next-line no-console
        console.log(event);
        return emitter({ type: SOCKET_MESSAGE_RECEIVED, payload: { event } });
      });

      sock.send(JSON.stringify({ type: 'PING' }));

      return emitter({ type: CREATE_SOCKET_SUCCESS, payload: { sock } });
    });

    sock.addEventListener('error', () => emitter({ type: CREATE_SOCKET_ERROR }));

    return () => {};
  });
};

const closeSocketChannel = function closeSocketChannel() {
  return eventChannel((emitter) => {
    sock.addEventListener('close', () => {
      // eslint-disable-next-line no-console
      console.log('Socket DOWN');
      return emitter({ type: CLOSE_SOCKET_SUCCESS });
    });

    sock.close(1000, 'Closing socket');

    return () => {};
  });
};

export function* createSocket() {
  const channel = yield call(initSockChannel);

  while (true) {
    const action = yield take(channel);
    yield put(action);
  }
}

export function* sendMessage(action) {
  const { message } = action.payload;

  if (sock.readyState === sock.OPEN) {
    sock.send(JSON.stringify(message));
    yield put({ type: SOCKET_SEND_SUCCESS });
  } else {
    yield put({ type: SOCKET_SEND_ERROR });
  }
}

export function* closeSocket() {
  const channel = yield call(closeSocketChannel);

  let action = {};
  while (action.type !== CLOSE_SOCKET_SUCCESS) {
    action = yield take(channel);
    yield put(action);
  }
}

export default function* roomsSaga() {
  yield takeLatest(CREATE_SOCKET_REQUESTED, createSocket);
  yield takeEvery(SOCKET_SEND_REQUESTED, sendMessage);
  yield takeLatest(CLOSE_SOCKET_REQUESTED, closeSocket);
}

import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_CONVERSATION_REQUESTED, FETCH_CONVERSATION_SUCCESS } from '../actions/types';
import { /* httpGet */ later } from './utils';

export function* fetchConversation(action) {
  yield later(1000);
  yield put({
    type: FETCH_CONVERSATION_SUCCESS,
    payload: {
      id: 1,
      roomId: action.payload.roomId,
      loading: false,
      messages: [
        {
          id: 1, userId: 1, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
        {
          id: 2, userId: 1, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
        {
          id: 3, userId: 3, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
        {
          id: 4, userId: 1, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
        {
          id: 5, userId: 2, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
        {
          id: 6, userId: 3, timestamp: Date.now(), content: 'Lorem ipsum dolor sit amet',
        },
      ],
    },
  });
}

export default function* conversationSaga() {
  yield takeLatest(FETCH_CONVERSATION_REQUESTED, fetchConversation);
}

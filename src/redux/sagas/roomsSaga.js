import { put, takeLatest } from 'redux-saga/effects';
import { FETCH_ROOMS_REQUESTED, FETCH_ROOMS_SUCCESS } from '../actions/types';
import { later /* , httpGet */ } from './utils';

export function* fetchRooms(/* action */) {
  // const data = yield httpGet('http://www.mocky.io/v2/5bc385983100002b001fcbd9?mocky-delay=500ms', {
  //   defaultValue: { rooms: [] },
  // });
  yield later(1000);
  yield put({
    type: FETCH_ROOMS_SUCCESS,
    payload: {
      rooms: [
        { id: 5, name: 'Demo loaded' },
        { id: 66, name: 'Other demo loaded room' },
        { id: 123, name: 'A room with a very very long name that will surely break the layout' },
        { id: 67, name: 'Other demo loaded room' },
        { id: 68, name: 'Other demo loaded room' },
        { id: 69, name: 'Other demo loaded room' },
        { id: 80, name: 'Other demo loaded room' },
        { id: 88, name: 'Other demo loaded room' },
      ],
    },
  });
}

export default function* roomsSaga() {
  yield takeLatest(FETCH_ROOMS_REQUESTED, fetchRooms);
}

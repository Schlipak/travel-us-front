import { put, takeEvery } from 'redux-saga/effects';
import { later } from './utils';

import { SEND_MESSAGE_REQUESTED, SEND_MESSAGE_SUCCESS } from '../actions/types';

export function* incrementLater(action) {
  yield later(1000);
  yield put({ type: SEND_MESSAGE_SUCCESS, payload: action.payload });
}

export default function* incrementSaga() {
  yield takeEvery(SEND_MESSAGE_REQUESTED, incrementLater);
}

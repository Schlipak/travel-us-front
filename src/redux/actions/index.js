import {
  INCREMENT_COUNT,
  FETCH_ROOMS_REQUESTED,
  FETCH_CONVERSATION_REQUESTED,
  RESET_COUNT,
  SEND_MESSAGE_REQUESTED,
  FETCH_CURRENT_USER_REQUESTED,
  LOGIN_CURRENT_USER_REQUESTED,
  LOGOUT_CURRENT_USER_REQUESTED,
  CREATE_SOCKET_REQUESTED,
  CLOSE_SOCKET_REQUESTED,
  SIGN_UP_CURRENT_USER_REQUESTED,
  SOCKET_SEND_REQUESTED,
} from './types';

export * from './types';

export const incrementCount = () => ({
  type: INCREMENT_COUNT,
});

export const resetCount = () => ({
  type: RESET_COUNT,
});

export const fetchRooms = () => ({
  type: FETCH_ROOMS_REQUESTED,
});

export const fetchConversation = roomId => ({
  type: FETCH_CONVERSATION_REQUESTED,
  payload: { roomId },
});

export const sendMessage = (message, roomId, callback = () => {}) => ({
  type: SEND_MESSAGE_REQUESTED,
  payload: { roomId, message, callback },
});

export const fetchCurrentUser = () => ({
  type: FETCH_CURRENT_USER_REQUESTED,
});

export const signUpUser = (username, password) => ({
  type: SIGN_UP_CURRENT_USER_REQUESTED,
  payload: { username, password },
});

export const loginCurrentUser = (username, password) => ({
  type: LOGIN_CURRENT_USER_REQUESTED,
  payload: { username, password },
});

export const logoutCurrentUser = () => ({
  type: LOGOUT_CURRENT_USER_REQUESTED,
});

export const createSocket = () => ({
  type: CREATE_SOCKET_REQUESTED,
});

export const socketSend = message => ({
  type: SOCKET_SEND_REQUESTED,
  payload: { message },
});

export const closeSocket = () => ({
  type: CLOSE_SOCKET_REQUESTED,
});

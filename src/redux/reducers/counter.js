import { INCREMENT_COUNT_DONE, RESET_COUNT } from '../actions/types';

const initialState = {
  count: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
  case INCREMENT_COUNT_DONE:
    return { ...state, count: state.count + 1 };
  case RESET_COUNT:
    return { ...state, count: 0 };
  default:
    return state;
  }
};

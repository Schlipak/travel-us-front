import { FETCH_ROOMS_REQUESTED, FETCH_ROOMS_SUCCESS } from '../actions/types';

const initialState = {
  loading: false,
  elements: [{ id: 1, name: 'Test' }, { id: 2, name: 'Other' }],
};

export default (state = initialState, action) => {
  let newIDs = [];

  if (action.payload && action.payload.rooms) {
    newIDs = action.payload.rooms.map(room => room.id);
  }

  switch (action.type) {
  case FETCH_ROOMS_REQUESTED:
    return {
      loading: true,
      elements: state.elements,
    };
  case FETCH_ROOMS_SUCCESS:
    return {
      loading: false,
      elements: [
        ...state.elements.filter(room => !newIDs.includes(room.id)),
        ...action.payload.rooms,
      ],
    };
  default:
    return state;
  }
};

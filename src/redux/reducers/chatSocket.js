import {
  CREATE_SOCKET_REQUESTED,
  CREATE_SOCKET_SUCCESS,
  CREATE_SOCKET_ERROR,
  CLOSE_SOCKET_REQUESTED,
  CLOSE_SOCKET_SUCCESS,
  CLOSE_SOCKET_ERROR,
  SOCKET_SEND_REQUESTED,
  SOCKET_SEND_SUCCESS,
  SOCKET_SEND_ERROR,
} from '../actions/types';

export const Status = {
  DISCONNECTED: 'DISCONNECTED',
  CONNECTING: 'CONNECTING',
  CONNECTION_ERROR: 'CONNECTION_ERROR',
  CONNECTED: 'CONNECTED',
  DISCONNECTING: 'DISCONNECTING',
  DISCONNECTION_ERROR: 'DISCONNECTION_ERROR',
  IDLE: 'IDLE',
};

const initialState = {
  sock: null,
  status: Status.DISCONNECTED,
};

export default (state = initialState, action) => {
  switch (action.type) {
  case CREATE_SOCKET_REQUESTED:
    return { ...state, status: Status.CONNECTING };
  case CREATE_SOCKET_SUCCESS:
    return { ...state, sock: action.payload.sock, status: Status.CONNECTED };
  case CREATE_SOCKET_ERROR:
    return { ...state, sock: null, status: Status.CONNECTION_ERROR };

  case SOCKET_SEND_REQUESTED:
  case SOCKET_SEND_SUCCESS:
  case SOCKET_SEND_ERROR:
    return state;

  case CLOSE_SOCKET_REQUESTED:
    return state.status === Status.DISCONNECTED
      ? state
      : { ...state, status: Status.DISCONNECTING };
  case CLOSE_SOCKET_SUCCESS:
    return { ...state, sock: null, status: Status.DISCONNECTED };
  case CLOSE_SOCKET_ERROR:
    return { ...state, status: Status.DISCONNECTION_ERROR };
  default:
    return state;
  }
};

import {
  FETCH_CONVERSATION_REQUESTED,
  FETCH_CONVERSATION_SUCCESS,
  SEND_MESSAGE_SUCCESS,
} from '../actions';

const initialState = [];

export default (state = initialState, action) => {
  const newID = action.payload && action.payload.roomId;

  switch (action.type) {
  case FETCH_CONVERSATION_REQUESTED:
    return [
      ...state.filter(conv => conv.roomId !== newID),
      {
        id: 0,
        roomId: newID,
        loading: true,
        messages: [],
      },
    ];

  case FETCH_CONVERSATION_SUCCESS:
    return [...state.filter(conv => conv.roomId !== newID), action.payload];

  case SEND_MESSAGE_SUCCESS:
    return (() => {
      const roomId = action.payload && action.payload.roomId;
      const conv = state.find(c => c.roomId === roomId);

      const messages = [
        ...conv.messages,
        {
          id: Math.round(Math.random() * 1000000),
          userId: 1,
          timestamp: Date.now(),
          content: action.payload.message,
        },
      ];

      if (action.payload.callback) action.payload.callback();

      return [...state.filter(c => c.id !== conv.id), { ...conv, messages }];
    })();
  default:
    return state;
  }
};

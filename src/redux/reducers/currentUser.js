import {
  FETCH_CURRENT_USER_REQUESTED,
  FETCH_CURRENT_USER_SUCCESS,
  FETCH_CURRENT_USER_ERROR,
  LOGOUT_CURRENT_USER_REQUESTED,
  LOGOUT_CURRENT_USER_SUCCESS,
  LOGOUT_CURRENT_USER_ERROR,
  LOGIN_CURRENT_USER_REQUESTED,
  LOGIN_CURRENT_USER_SUCCESS,
  LOGIN_CURRENT_USER_ERROR,
  SIGN_UP_CURRENT_USER_REQUESTED,
  SIGN_UP_CURRENT_USER_SUCCESS,
  SIGN_UP_CURRENT_USER_ERROR,
} from '../actions/types';

export const Status = {
  LOGGED_OUT: 'LOGGED_OUT',
  LOGGED_IN: 'LOGGED_IN',
  LOG_IN_ERROR: 'LOG_IN_ERROR',
  LOG_OUT_ERROR: 'LOG_OUT_ERROR',
  SIGN_UP_ERROR: 'SIGN_UP_ERROR',
};

const initialState = {
  loading: false,
  status: Status.LOGGED_OUT,
  user: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
  case FETCH_CURRENT_USER_REQUESTED:
    return { ...state, loading: true };
  case FETCH_CURRENT_USER_SUCCESS:
    return {
      ...state,
      loading: false,
      status: Status.LOGGED_IN,
      user: action.payload.user,
    };
  case FETCH_CURRENT_USER_ERROR:
    return {
      ...state,
      loading: false,
      status: Status.LOG_IN_ERROR,
      user: null,
    };

  case LOGIN_CURRENT_USER_REQUESTED:
    return { ...state, loading: true };
  case LOGIN_CURRENT_USER_SUCCESS:
    return {
      ...state,
      loading: false,
      user: action.payload.user,
      status: Status.LOGGED_IN,
    };
  case LOGIN_CURRENT_USER_ERROR:
    return {
      ...state,
      loading: false,
      status: Status.LOG_IN_ERROR,
      user: null,
    };

  case LOGOUT_CURRENT_USER_REQUESTED:
    return { ...state, loading: true };
  case LOGOUT_CURRENT_USER_SUCCESS:
    return {
      ...state,
      loading: false,
      status: Status.LOGGED_OUT,
      user: null,
    };
  case LOGOUT_CURRENT_USER_ERROR:
    return { ...state, loading: false, status: Status.LOG_OUT_ERROR };

  case SIGN_UP_CURRENT_USER_REQUESTED:
    return { ...state, loading: true };
  case SIGN_UP_CURRENT_USER_SUCCESS:
    return {
      ...state,
      loading: false,
      status: Status.LOGGED_IN,
      user: action.payload.user,
    };
  case SIGN_UP_CURRENT_USER_ERROR:
    return { ...state, loading: false, status: Status.SIGN_UP_ERROR };

  default:
    return state;
  }
};

import { combineReducers } from 'redux';

import currentUser from './currentUser';
import chatSocket from './chatSocket';
import counter from './counter';
import rooms from './rooms';
import conversations from './conversations';

export default combineReducers({
  currentUser,
  chatSocket,
  counter,
  rooms,
  conversations,
});

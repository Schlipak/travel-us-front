const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');

module.exports = function override(config, env) {
  const injectedConfig = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
    config,
  );

  const rewiredConfig = rewireLess.withLoaderOptions({
    modifyVars: {
      '@primary-color': '#20bf6b',
      '@success-color': '#26de81',
      '@warning-color': '#fd9644',
      '@error-color': '#eb3b5a',
      '@highlight-color': '#fc5c65',
    },
    javascriptEnabled: true,
  })(injectedConfig, env);

  return rewiredConfig;
};
